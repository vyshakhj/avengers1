package com.avengers;

import com.avengers.models.Book;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BooksController {
    @RequestMapping("/")
    public List<Book> books() {
        List<Book> books = new ArrayList<>();
        books.add(new Book("Book shop 1"));
        return books;
    }
}
