
FROM openjdk:8-alpine
ADD /build/libs/avengers-shopping-cart-api-1.0.jar avengers-shopping-cart-api.jar
ENTRYPOINT ["java", "-jar", "/avengers-shopping-cart-api.jar"]